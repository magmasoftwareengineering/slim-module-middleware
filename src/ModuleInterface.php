<?php

declare(strict_types=1);

namespace MagmaSoftwareEngineering\Slim\Module;

/**
 * Interface Module
 * @package MagmaSoftwareEngineering\Slim\Module
 */
interface ModuleInterface
{
    public function getAutoLoadConfig(): array;
}

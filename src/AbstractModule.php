<?php

declare(strict_types=1);

namespace MagmaSoftwareEngineering\Slim\Module;

/**
 * @psalm-suppress UnusedClass
 */
abstract class AbstractModule implements ModuleInterface
{
    /**
     * @return array
     * @throws \ReflectionException
     */
    public function getAutoLoadConfig(): array
    {
        // Child class __DIR__ (Late static binding)
        $class_info = new \ReflectionClass($this);
        $__DIR__ = dirname($class_info->getFileName());

        // Child class __NAMESPACE__ (Late static binding)
        $__NAMESPACE__ = $class_info->getNamespaceName();

        return [
            $__NAMESPACE__ => $__DIR__ . DIRECTORY_SEPARATOR . 'src'
        ];
    }
}

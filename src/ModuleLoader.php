<?php

declare(strict_types=1);

namespace MagmaSoftwareEngineering\Slim\Module;

use Composer\Autoload\ClassLoader;
use Fig\Http\Message\StatusCodeInterface;
use FilesystemIterator;
use InvalidArgumentException;
use Psr\Container\ContainerInterface as Container;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use RecursiveCachingIterator;
use RecursiveCallbackFilterIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RuntimeException;
use Slim\App;
use Slim\Psr7\Response as SlimResponse;
use SplFileInfo;

use function array_key_exists;
use function error_reporting;
use function in_array;
use function is_array;

use const E_ALL;
use const E_DEPRECATED;
use const E_STRICT;

/**
 * Class Module
 * @package MagmaSoftwareEngineering\Slim\Module
 */
final class ModuleLoader
{
    public const MODULE_FILE_NAME = 'Module.php';

    private static array $directoryExcludes = [
        '.',
        '..',
        '.git',
        '.idea',
        '.vscode',
        'src',
        'vendor',
    ];

    private ?LoggerInterface $logger = null;

    private string $modulePath = '';

    private array $options = [
        'app' => '',
        'container' => '',
        'modulesPath' => [],
        'modules' => [],
        'middleware' => [],
        'preLoadModules' => [],
    ];

    private bool $initialised = false;

    /**
     * Module constructor.
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function __construct(array $options = [])
    {
        if (!empty($options)) {
            $this->init($options);
        }
    }

    public function init(array $options): self
    {
        if (array_key_exists('app', $options) && !$options['app'] instanceof App) {
            throw new InvalidArgumentException('Invalid option "app", please supply \Slim\App');
        }
        if (array_key_exists('container', $options) && !$options['container'] instanceof Container) {
            throw new InvalidArgumentException(
                'Invalid option "container", please supply \Psr\Container\Interface instance'
            );
        }

        /* Store passed in options overwriting any defaults. */
        $this->hydrate($options)
            ->scanModulePaths();

        $this->initialised = true;

        return $this;
    }

    /**
     * Hydrate options from given array
     */
    private function hydrate(array $data = []): self
    {
        foreach ($data as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }

        return $this;
    }

    private function setModulePath(string $modulePath = ''): self
    {
        $this->modulePath = $modulePath;

        return $this;
    }

    public static function modulePaths(
        string $modulePath = '',
        string $match = '/container.php',
        array $dirExcludes = []
    ): array {
        $modulePaths = [];
        if (empty($dirExcludes)) {
            $dirExcludes = self::$directoryExcludes;
        }
        $directory = new RecursiveDirectoryIterator(
            $modulePath,
            FilesystemIterator::SKIP_DOTS | FilesystemIterator::FOLLOW_SYMLINKS
        );
        $filter = new RecursiveCallbackFilterIterator(
            $directory,
            function (
                SplFileInfo $current,
                string $key,
                RecursiveDirectoryIterator $iterator
            ) use (
                $dirExcludes,
                $match
            ): bool {
                if ($iterator->hasChildren() && !in_array($current->getFilename(), $dirExcludes, true)) {
                    return true;
                }

                if (str_contains($current->getPathname(), $match)) {
                    return true;
                }

                return false;
            }
        );
        $iterator = new RecursiveCachingIterator($filter);
        $iterator = new RecursiveIteratorIterator($iterator);
        /** @var SplFileInfo $file */
        foreach ($iterator as $file) {
            $dir = dirname($file->getPathname());
            if (!in_array($dir, $modulePaths, true)) {
                $modulePaths[] = $dir;
            }
        }
        return $modulePaths;
    }

    /**
     * Recursively scan module paths looking for valid modules
     */
    private function scanModulePaths(array $modulesPath = []): self
    {
        if (empty($modulesPath)) {
            $modulesPath = $this->options['modulesPath'];
        }

        if (count($modulesPath)) {
            $dirExcludes = self::$directoryExcludes;
            foreach ($modulesPath as $modulePath) {
                $moduleSubPaths = self::modulePaths($modulePath, '/Module.php', $dirExcludes);
                foreach ($moduleSubPaths as $subPath) {
                    if (!is_readable($subPath . '/' . self::MODULE_FILE_NAME)) {
                        throw new RuntimeException(
                            'Module: ' . $subPath . ': Failed to load required ' . self::MODULE_FILE_NAME
                        );
                    }

                    $this->log(LogLevel::DEBUG, 'Module: ' . $subPath . ': Loading ' . self::MODULE_FILE_NAME);
                    require $subPath . '/' . self::MODULE_FILE_NAME;
                }
            }
        }

        return $this;
    }

    /**
     * Instantiate all the loaded modules
     */
    public function bootstrap(array $modules = null): self
    {
        /** @var Container $container */
        $container = $this->options['container'];

        if (null === $modules) {
            $modulesSettings = $container->get('modules.load');
            if (count($modulesSettings)) {
                // Late load the module settings
                $this->hydrate(['modules' => $modulesSettings]);
            }

            $modules = $this->options['modules'];
        }

        $this->log(LogLevel::INFO, 'Bootstrapping Modules');

        // Follow standard Slim load order across all modules.
        foreach ($modules as $moduleName) {
            $className = $moduleName . '\\Module';

            $this->log(LogLevel::INFO, 'Module: ' . $moduleName . '\Module');

            if (!class_exists($className)) {
                throw new RuntimeException('Module: ' . $className . ': does not exist');
            }

            /** @var ModuleInterface $module */
            $module = new $className();

            $config = $module->getAutoLoadConfig();
            /** @var ClassLoader $autoload */
            $autoload = $container->get(ClassLoader::class);
            if ($autoload->findFile($className) !== false) {
                $autoload->addPsr4($moduleName . '\\', $config[$moduleName]);
            }

            // Set module path to configured module path without the last directory - could be problematic?
            $this->setModulePath(substr($config[$moduleName], 0, strrpos($config[$moduleName], '/')));
        }
        foreach ($modules as $moduleName) {
            $className = $moduleName . '\\Module';

            if (!class_exists($className)) {
                throw new RuntimeException('Module: ' . $className . ': does not exist');
            }

            /** @var ModuleInterface $module */
            $module = new $className();

            $config = $module->getAutoLoadConfig();

            $this->setModulePath(substr($config[$moduleName], 0, strrpos($config[$moduleName], '/')));

            $this->loadFile('routes.php');
        }
        foreach ($modules as $moduleName) {
            $className = $moduleName . '\\Module';

            if (!class_exists($className)) {
                throw new RuntimeException('Module: ' . $className . ': does not exist');
            }

            /** @var ModuleInterface $module */
            $module = new $className();

            $config = $module->getAutoLoadConfig();

            $this->setModulePath(substr($config[$moduleName], 0, strrpos($config[$moduleName], '/')));

            $this->loadFile('middleware.php');
        }

        return $this;
    }

    /**
     * Load general Slim bootstrap files
     *
     * @psalm-suppress UnusedVariable
     */
    private function loadFile(string $file): array
    {
        if (is_readable($this->modulePath . '/' . $file)) {
            /** @var App $app */
            $app = $this->options['app'];
            /** @var Container $container */
            $container = $this->options['container'];

            $this->log(LogLevel::DEBUG, 'Module: ' . $this->modulePath . ': Loading ' . $file);
            $errorReporting = error_reporting();
            error_reporting(E_ALL & ~E_DEPRECATED | E_STRICT);
            $return = require $this->modulePath . '/' . $file;
            error_reporting($errorReporting);
            if (is_array($return)) {
                return $return;
            }
        }
        return [];
    }

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function setApp(App $app): self
    {
        $this->options['app'] = $app;

        return $this;
    }

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function setContainer(Container $container): self
    {
        $this->options['container'] = $container;

        return $this;
    }

    /**
     * Set directories where modules reside
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function setModulesPath(array $paths): self
    {
        $this->options['modulesPath'] = $paths;

        return $this;
    }

    /**
     * Which modules are we going to preload
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function setPreLoadModules(array $modules = []): self
    {
        $this->options['preLoadModules'] = $modules;

        return $this;
    }

    /**
     * Which modules are we going to load
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function setModules(array $modules = []): self
    {
        $this->options['modules'] = $modules;

        return $this;
    }

    /**
     * Which middleware are we going to load
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function setMiddleware(array $middleware = []): self
    {
        $this->options['middleware'] = array_reverse($middleware);

        return $this;
    }

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function setDirectoryExcludes(array $directoryExcludes = []): self
    {
        self::$directoryExcludes = $directoryExcludes;

        return $this;
    }

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function addDirectoryExclude(string $directoryExclude = ''): self
    {
        self::$directoryExcludes[] = $directoryExclude;

        return $this;
    }

    /**
     * Get the logger
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function getLogger(): ?LoggerInterface
    {
        return $this->logger;
    }

    /**
     * Set the logger
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function setLogger(?LoggerInterface $logger = null): self
    {
        $this->logger = $logger;

        return $this;
    }

    /**
     * Logs with an arbitrary level
     *
     * @param mixed $level
     */
    private function log($level, string $message, array $context = []): void
    {
        if (isset($this->logger)) {
            $this->logger->log($level, $message, $context);
        }
    }
}
